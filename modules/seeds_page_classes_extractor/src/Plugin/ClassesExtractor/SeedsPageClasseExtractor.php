<?php

namespace Drupal\seeds_page_classes_extractor\Plugin\ClassesExtractor;

use Drupal\classes_extractor\ClassesExtractorBase;
use Drupal\classes_extractor\ClassesExtractorManager;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a Seeds Page Classes Extractor plugin.
 *
 * @ClassesExtractor(
 *   id = "seeds_page_classe_extractor",
 *   label = @Translation("Seeds Page Classes Extractor")
 * )
 */
class SeedsPageClasseExtractor extends ClassesExtractorBase implements ContainerFactoryPluginInterface {

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;


  /**
   * The entity type manager service.
   *
   * @var \Drupal\classes_extractor\ClassesExtractorManager
   */
  protected $classExtractorManager;

  /**
   * Constructs an EntityDisplayClassesExtractor object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $class_extractor_manager
   *   The entity type manager service.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, ClassesExtractorManager $class_extractor_manager) {
    $this->entityTypeManager = $entity_type_manager;
    $this->classExtractorManager = $class_extractor_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('classes_extractor.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getClasses(): array {
    $classes = [];
    // Extract classes from Seeds Page Block Field module if available.
    $entityFieldManager = \Drupal::service('entity_field.manager');
    $fieldDefinitions = $entityFieldManager->getFieldDefinitions('block_content', 'seeds_grid');

    $targetFields = [
      $fieldDefinitions['field_seeds_desktop'] ?? NULL,
      $fieldDefinitions['field_seeds_tablet'] ?? NULL,
      $fieldDefinitions['field_seeds_mobile'] ?? NULL,
    ];

    foreach ($targetFields as $field) {
      if ($field) {
        $allowedValues = array_keys($field->getFieldStorageDefinition()->getSetting('allowed_values'));
        $classes = array_merge($classes, $allowedValues);
      }
    }
    return $classes;
  }

}
